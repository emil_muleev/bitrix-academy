<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$linkArray = array();
foreach ($arResult["ITEMS"] as $ID => $arItems) {
    $arImage = CFile::ResizeImageGet($arItems["PREVIEW_PICTURE"], array('width' => $arParams["LIST_PREV_PICT_W"], 'height' => $arParams["LIST_PREV_PICT_H"]), BX_RESIZE_IMAGE_PROPORTIONAL, true);
    $arResult["ITEMS"][$ID]["PREVIEW_PICTURE"] = $arImage;
    $linkArray[] = $arItems["PROPERTIES"]["LINK"]["VALUE"];
}

$arSelect = Array("ID", "NAME", "DETAIL_PAGE_URL", "PROPERTY_PRICE");
$arFilter = Array("IBLOCK_ID" => CATALOG_IBLOCK_ID, "ID" => $linkArray);
$db_elements = CIblockElement::GetList($arOrder, $arFilter, false, false, $arSelect);

$arProduct = array_fill_keys($linkArray, '');
while ($obElement = $db_elements->GetNextElement()) {
    $el = $obElement->GetFields();
    $el["PROPERTIES"] = $obElement->GetProperties();
    $arProduct[$el["ID"]] = $el;
}

foreach ($arResult["ITEMS"] as $ID => $arItems) {
    $arResult["ITEMS"][$ID]["PRODUCT"] = $arProduct[$arItems["PROPERTIES"]["LINK"]["VALUE"]];
}









