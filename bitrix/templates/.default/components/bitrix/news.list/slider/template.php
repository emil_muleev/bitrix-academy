<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<div class="sl_slider" id="slides">
    <div class="slides_container">
        <? foreach ($arResult["ITEMS"] as $arItems): ?>
            <div>
                <div>
                    <? if (is_array($arItems["PREVIEW_PICTURE"])): ?>
                        <img src="<?= $arItems["PREVIEW_PICTURE"]["src"] ?>" alt=""/>
                    <? endif; ?>
                    <h2><a href="<?= $arItems["PROPERTIES"]['LINK']['VALUE'] ?>"
                           title="<?= $arItems["PRODUCT"]["NAME"] ?>"><? echo $arItems["NAME"] ?></a></h2>

                    <p><? echo $arItems["PREVIEW_TEXT"]; ?></p>

                    <p><? echo($arItems["PRODUCT"]["NAME"] . " всего за " . $arItems["PRODUCT"]["PROPERTY_PRICE_VALUE"] . " руб."); ?></p>

                    <a href="<?= $arItems["PRODUCT"]["DETAIL_PAGE_URL"] ?>" title="<?= $arItems["PRODUCT"]["NAME"] ?>"
                       class="sl_more">Подробнее &rarr;</a>
                </div>
            </div>
        <? endforeach; ?>
    </div>
</div>





