<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
IncludeTemplateLangFile(__FILE__);
?>

<!DOCTYPE HTML>
<html lang="<?= LANGUAGE_ID ?>">
<head>
    <? $APPLICATION->ShowHead(); ?>

    <title><? $APPLICATION->ShowTitle() ?></title>

    <?
    $APPLICATION->SetAdditionalCSS("/bitrix/templates/.default/template_style.css");

    $APPLICATION->AddHeadScript("/bitrix/templates/.default/js/jquery-1.8.2.min.js");
    $APPLICATION->AddHeadScript("/bitrix/templates/.default/js/slides.min.jquery.js");
    $APPLICATION->AddHeadScript("/bitrix/templates/.default/js/functions.js");
    $APPLICATION->AddHeadScript("/bitrix/templates/main/js/jquery.carouFredSel-6.1.0-packed.js");
    $APPLICATION->AddHeadScript("/bitrix/templates/main/js/slides.min.jquery.js");
    $APPLICATION->AddHeadScript("/bitrix/templates/main/js/script.js");
    ?>

    <link rel="shortcut icon" type="image/x-icon" href="/bitrix/templates/.default/favicon.ico"/>

    <!--[if gte IE 9]>
    <style type="text/css">.gradient {
        filter: none;
    }</style><![endif]-->
</head>
<body>
<? $APPLICATION->ShowPanel(); ?>
<div class="wrap">
    <div class="hd_header_area">
        <? include_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/templates/.default/include/header.php"); ?>
    </div>

    <!--- // end header area --->

    <? $APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"slider", 
	array(
		"COMPONENT_TEMPLATE" => "slider",
		"IBLOCK_TYPE" => "stock",
		"IBLOCK_ID" => "6",
		"NEWS_COUNT" => "5",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "PRICE",
			1 => "LINK",
			2 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Акции",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"LIST_PREV_PICT_W" => "500",
		"LIST_PREV_PICT_H" => "500"
	),
	false
); ?>



    <!--- // end slider area --->

    <div class="main_container homepage">